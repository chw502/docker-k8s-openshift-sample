# Kubernetes Sample

To install Kubernetes, please refer to offical guide: https://kubernetes.io/docs/tasks/tools/install-kubectl/. Kubernetes is also included in `Docker Desktop`.

## To start application in Kubernetes with NETWORK docker image

1. Run `cd depolyment`.
1. Update image name `spec.template.spec.conatiners.image` in `deploymen.yaml`. Local image name can be found by `docker image ls`.
2. Run `kubectl apply -f configmap/config-configmap.yaml`
2. Run `kubectl apply -f configmap/config-application.yaml`
2. Run `kubectl create -f deployment.yaml`
3. Run `kubectl get deployment` to check whether deployment is executed.
4. Run `kubectl create -f service.yaml`
5. Rnu `kubectl get service` to check whether service is running.
6. Check http://localhost:30001/test/index.html to verify the application.
7. Run `kubectl delete serivces <service name>` and `kubectl delete deployment <deployment name>` to stop the serivce and deployment.

## To start application in Kubernetes with LOCAL docker image

1. Follow docker guide to build local docker image.
1. Run `cd depolyment`.
1. Update image name `spec.template.spec.conatiners.image` in `deploymen-local-docker.yaml`. Local image name can be found by `docker image ls`.
2. Run `kubectl create -f deployment-local-docker.yaml`
3. Run `kubectl get deployment` to check whether deployment is executed.
4. Run `kubectl create -f service.yaml`
5. Rnu `kubectl get service` to check whether service is running.
6. Check http://localhost:30001/test/index.html to verify the application.
7. Run `kubectl delete serivces <service name>` and `kubectl delete deployment <deployment name>` to stop the serivce and deployment.

## Kubernetes

Kubernetes (K8s) is a system for automating deployment, scaling, and management of containerized applications. Below is a simple Kubernetes architecture:

![](./img/kubernetes.png)

### Node

A node is a worker **machine** in Kubernetes. A node may be a VM or physical machine, depending on the cluster. Each node contains the services necessary to run pods and is managed by the master components. The services on a node include the container runtime, kubelet and kube-proxy.

<img style="width: 400px;" src="./img/node.PNG" />

### Pod

Pods are the smallest deployable units of computing that can be created and managed in Kubernetes. Pod may contain one or more containers. Pod may be considered as continaer in Docker.

### Deployment

Deployment manages Pod and ReplicaSet. Pods can be create by deployment.yaml

<img style="width: 300px;" src="./img/deployment.png" />

Sample deployment.yaml:

```yaml
spec:
  replicas: 3 # create 3 replicas
  template:
    metadata:
      labels:
        app: nginx # label can be selected by serivce
    spec:
      containers:
      - name: nginx
        image: nginx:1.10.2  # Image to be deployed
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 80 # Port which service is running on
```

### Service

Service allows access to pod. It defines:

- Logical set of Pods by label

    Defines set of Pods by `spec.select`
    ```yaml
   spec:
     selector:
       app: nginx
    ```
- Policy to access them

    Port and protocol definition on how external client can access the Pods
    ```yaml
    spec:
      ports:
      - name: http
        # the port for client to access
        port: 80
        # the port that web server listen on
        targetPort: 80
        # TCP/UDP are available, default is TCP
        protocol: TCP
      - name: https
        port: 443
        targetPort: 443
        protocol: TCP
    ```

### Externalize Spring Boot configuration with Configmap

To externalize Spring Boot configuration in K8S, there are 2 ways to do that: 

- ConfigMaps as Environment Variable
- Mounting ConfigMaps as file

#### ConfigMaps as Environment Variable

1. Define ConfigMap

    ```yaml
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: config-configmap # map to spec.container.env.valueFrom.configMapKeyRef
      namespace: default
    data:
      greeter.prefix: value from configmap # deployment.yaml will set this to env var
    ```

2. Define in deployment.yaml

    ```yaml
      spec:
          containers:
          - env:
            # Create 'GREETING_PREFIX' env variable in K8s
            - name: GREETING_PREFIX
              valueFrom:
                configMapKeyRef:
                  # read from configmap 'config-configmap'
                  name: config-configmap
                  key: greeter.prefix # read data 'greeter.prefix' in configmap 'config-configmap'
    ```

3. Read environment variable in Spring Boot

    ```java
    @RequestMapping("/greetfromproperties")
    public String greet() {
        // read environment variable
        String prefix = System.getenv().getOrDefault("GREETING_PREFIX", "Default value");

        return prefix;
    }    
    ```

#### Mounting ConfigMaps as file

The data in ConfigMap will be created as a file and store in defined Path. Spring Boot will read the file in pod.

1. Define ConfigMap

    A file will be generated from this ConfigMap. Content will be value in `data.application.properties`.

    ```yaml
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: config-application
      namespace: default
    data:
      # will read ${MESSAGE} (2nd line last word) from MESSAGE env variable
      application.properties: >
        greeter.message=Using configmap mounted application.properties- <br/> 
        MESSAGE from configmap env variable: <span style="color:red">${MESSAGE}</span> 
    ```  

2. Define in deployment.yaml

    Deployment.yaml will define few things:

    1. Read data from **which** ConfigMap
    2. Store the data as file to **where**

    ```yaml
        spec:
          containers:
              env:
              # Define env var will be used in application.properties
              - name: MESSAGE
                valueFrom:
                  configMapKeyRef:
                    name: config-configmap
                    key: message
    #---- 2. Define mount to where ----------------
              volumeMounts: 
              - name: application-config 
                # mount to this path
                mountPath: /usr/local/tomcat/bin/application.properties
                subPath: application.properties 
    #---- 1. Define mount file from which ConfigMap 
          volumes: 
          - name: application-config
            configMap:
              # name of configmap to be mounted
              name: config-application
              items:
              # Store all content in ConfigMap 'application.properties' key to '{mountPath}/application.properties'
              - key: application.properties 
                path: application.properties     
    ```

3. Read value in Spring Boot

    In deployment.yaml, define Spring Boot `SPRING_CONFIG_LOCATION` variable to read application.properties.

    ```yaml
        spec:
          containers:
              env:
              - name: SPRING_CONFIG_LOCATION # let Tomcat know where to find application.properties
    ```

### Kubernetes Console

1. Run the following command
    ```
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta1/aio/deploy/recommended.yaml
    ```

2. Start console
    ```
    kubectl proxy
    ```

3. Check token 
    ```
    kubectl describe secrets
    ```
    
4. Visit http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

Ref
- [Kubernetes 101: Pods, Nodes, Containers, and Clusters](https://medium.com/google-cloud/kubernetes-101-pods-nodes-containers-and-clusters-c1509e409e16)
- [Kubernetes Service 概念詳解](https://tachingchen.com/tw/blog/kubernetes-service/)
- [Kubernetes interactive tutorial](https://www.katacoda.com/courses/kubernetes)
- [Configuring Spring Boot on Kubernetes with ConfigMap](https://developers.redhat.com/blog/2017/10/03/configuring-spring-boot-kubernetes-configmap/#deploy-application)
- [haitlc.home(accessible in HA network)](http://haitlc.home/Cloud/Cloud%20Technology/Container%20Management.html)
