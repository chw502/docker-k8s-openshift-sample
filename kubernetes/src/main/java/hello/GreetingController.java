package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
	
    @Value("${greeter.message}")
    private String greeterMessageFormat;

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return "Hello Docker World";
    }
    
    @RequestMapping("/greetfromproperties")
    public String greet() {
        String prefix = System.getenv().getOrDefault("GREETING_PREFIX", "Default value");
        if (prefix == null) {
            prefix = "Hello!";
        }
 
        return prefix + "<br/>" + greeterMessageFormat;
    }    
}