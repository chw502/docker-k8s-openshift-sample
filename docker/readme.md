# Docker Sample

To start with this sample, you need to have docker installed in your machine. You may refer to docker offical page for details:
https://docs.docker.com/v17.12/install/

## To start the application

1. Run `mvn package`.
2. Run `docker build -t <image name> .` to build docker image. Image name is totally up to you.
3. Run `docker image ls` to check whether image is built
4. Run `docker run -p 3000:8080 <image name>` to start the image in container.
5. Check http://localhost:3000/test/index.html to verify the application.
7. Check container id by `docker container ls`.
6. Run `docker container kill <container ID>` to stop container running.

Dockerfile is a set of commands and declarations which define docker image.


## Docker

Docker 是個輕量級的虛擬化技術，底層使用 cgroup、chroot、namespace 實作，可以把你的應用程式連同環境一起打包，部屬的時候就不用再擔心環境的問題. Docker 可以幫你把 Ubuntu + php7 + MySQL + Apache 的環境跟你的程式碼打包起來. Simply speaking, docker packages your Code + configuration + dependency + runtime together.

Container archiecture:
![](https://www.docker.com/sites/default/files/d8/2018-11/container-vm-whatcontainer_2.png)

## Docker Image

Docker image 指的是已經打包好的環境，但是還沒有跑起來，就是一堆檔案而已. 

## Container

Docker container is a instance of Docker image. Container 則是把 image 跑起來之後產生的 instance，他們之間的關係就像程式碼跟跑起來的程式.

## Dockerfile

Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image.

Ref:
- [Docker 實戰系列（一）：一步一步帶你 dockerize 你的應用 (in Nodejs)](https://larrylu.blog/step-by-step-dockerize-your-app-ecd8940696f4) 