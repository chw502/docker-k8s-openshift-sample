# Sample code deploys to Private Cloud

This project consists of 3 sub-projects. Each project provides runable to the corresponding tool/platform.

- Docker
- Kubernetes
- Openshift

All projects are with same spring-boot code buildable to war. The war consists of a index.html & REST API with GET routing:
- /index.html
- /greeting

## IDE/Development tools

- [Spring STS](https://spring.io/tools)
- Maven 3